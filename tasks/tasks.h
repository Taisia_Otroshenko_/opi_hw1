#ifndef MENU_TEST_TASKS_H
#define MENU_TEST_TASKS_H

namespace Tasks {
    void FillMatrixRandom();

    void OutMatrix();

    void MaxMultiplicationInRow();

    void Replace();

    void SortRowsInMatrixDescending();

    void MaxElementUnderTheSideDiagonal();
}

#endif //MENU_TEST_TASKS_H
