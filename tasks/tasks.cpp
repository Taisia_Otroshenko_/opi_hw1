#include <iostream>
#include <iomanip>
#include <climits>
#include <random>
#include "tasks.h"

namespace Tasks {
    const int ROW = 12;
    const int COL = 12;
    const int MAX_VALUE = 17;
    const int MIN_VALUE = -5;
    int MATRIX[ROW][COL]{};

    using namespace std;

    void FillMatrixRandom() {
        random_device rd;
        default_random_engine engine(rd());
        uniform_int_distribution<int> uniformDistribution(MIN_VALUE, MAX_VALUE);

        for (int i = 0; i < ROW; i++) {
            for (int j = 0; j < COL; j++) {
                MATRIX[i][j] = uniformDistribution(engine);
            }
        }
        cout << "Матрица " << ROW << "x" << COL << " заполнена (псевдо)случайными числами";
    }

    void OutMatrix() {
        for (int i = 0; i < ROW; i++) {
            for (int j = 0; j < COL; j++) {
                cout << setw(5) << MATRIX[i][j];
            }
            cout << endl;
        }
        cout << endl;
    }

    void MaxMultiplicationInRow() {
        int indexMax = -1;
        long long int resultMax = LLONG_MIN;
        for (int i = 0; i < ROW; i++) {
            long long int temp = 1;
            bool finded = false;
            for (int j = 0; j < COL; j++) {
                if (MATRIX[i][j] != 0) {
                    finded = true;
                    temp *= MATRIX[i][j];
                }
            }
            if (finded && temp > resultMax) {
                indexMax = i;
                resultMax = temp;
            }
        }
        if (indexMax != -1) {
            cout << "Номер строки в матрице с максимальным произведением: ";
            cout << indexMax + 1 << endl;
        } else {
            cout << "Не было найдено строки без нулей с максимальным произведением";
        }
    }

    void Replace() {
        for (int i = 0; i < ROW; ++i) {
            for (int j = 0; j < COL; ++j) {
                if (MATRIX[i][j] < 0) {
                    int square = MATRIX[i][j] * MATRIX[i][j];
                    MATRIX[i][j] = square;
                }
            }
        }

        cout << "Все элементы матрицы со значением меньше 0 заменены на собственные квадраты" << endl;
    }

    void Swap(int &a, int &b) {
        int tmp = a;
        a = b;
        b = tmp;
    }

    void SortRowsInMatrixDescending() {
        for (int i = 0; i < ROW; ++i) {
            for (int currentIndex = 0; currentIndex < COL - 1; ++currentIndex) {
                int indexMaxElement = currentIndex;
                for (int j = currentIndex + 1; j < COL; ++j) {
                    if (MATRIX[i][j] > MATRIX[i][indexMaxElement]) {
                        indexMaxElement = j;
                    }
                }
                Swap(MATRIX[i][currentIndex], MATRIX[i][indexMaxElement]);
            }

        }
        cout << "Каждая строка матрицы была отсортирована по убыванию" << endl;
    }

    void MaxElementUnderTheSideDiagonal() {
        int maxElement = INT_MIN;
        int maxElementRow = 0, maxElementCol = 0;
        for (int i = 0; i < ROW; ++i) {
            for (int j = 0; j < COL; ++j) {
                if (i + j >= min(ROW, COL) + 1 && MATRIX[i][j] > maxElement) {
                    maxElement = MATRIX[i][j];
                    maxElementRow = i + 1;
                    maxElementCol = j + 1;
                }
            }
        }
        cout << "Номер максимального элемента под побочной диагональю: ";
        cout << maxElementRow << " " << maxElementCol << endl;
    }
}
