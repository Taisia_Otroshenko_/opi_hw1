#include <regex>
#include "../CLI-Autocomplete/include/autocomplete.h"
#include "constants.h"

namespace utility {
    std::string coloredMessage(const std::string &message, const std::string &color) {
        return set_console_color(color) + message + set_console_color(constants::default_color);
    }

    void clearScreen() {
        system("clear");
    }

    constants::CMD _cmdToCode(const std::string &cmd) {
        constants::CMD retCode = constants::CMD::ERROR;
        if (regex_match(cmd, std::regex(R"(^\s*clear\s*$)"))) {
            retCode = constants::CMD::CLEAR;
        } else if (regex_match(cmd, std::regex(R"(^\s*exit\s*$)"))) {
            retCode = constants::CMD::EXIT;
        } else if (regex_match(cmd, std::regex(R"(^\s*task\s+.+\s*$)"))) {
            retCode = constants::CMD::TASK;
        }
        return retCode;
    }

    void removeSpaces(std::string &str) {
        str.erase(std::unique_copy(str.begin(), str.end(), str.begin(),
                                   [](char c1, char c2) { return c1 == ' ' && c2 == ' '; }),
                  str.end());

        if (isspace(str[str.size() - 1])) {
            str.pop_back();
        }
        if (isspace(str[0])) {
            str.erase(0, 1);
        }
    }
}
