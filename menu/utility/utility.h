#ifndef MENU_TEST_UTILITY_H
#define MENU_TEST_UTILITY_H

namespace utility {
    std::string coloredMessage(const std::string &message, const std::string &color);
    void clearScreen();
    constants::CMD _cmdToCode(const std::string &cmd);
    void removeSpaces(std::string &str);
}

#endif //MENU_TEST_UTILITY_H
