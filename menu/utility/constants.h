#ifndef MENU_TEST_CONSTANTS_H
#define MENU_TEST_CONSTANTS_H

#include <string>

namespace constants {
    extern const std::string config_file_path;
    extern const std::string optional_brackets;
    extern const std::string line_prompt;
    extern const std::string title_color;
    extern const std::string predict_color;
    extern const std::string default_color;
    extern const std::string err_color;
    enum class CMD : int8_t {
        ERROR = 0,
        CLEAR,
        EXIT,
        TASK,
    };
}


#endif //MENU_TEST_CONSTANTS_H
