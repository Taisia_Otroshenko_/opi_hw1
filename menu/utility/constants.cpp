#include <string>

namespace constants {
    extern const std::string config_file_path{"../config_autocomplete.txt"};
    extern const std::string optional_brackets{"["};
    extern const std::string line_prompt{"Input command:"};
    extern const std::string title_color{"0;32;49"};
    extern const std::string predict_color{"90"};
    extern const std::string default_color{"0"};
    extern const std::string err_color{"31"};
}
