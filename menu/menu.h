#pragma once

#include <iostream>
#include <string>
#include <utility>

namespace UserMenu {
    class Menu {
    public:
        using function = void (*)();

        [[maybe_unused]] void setTitle(std::string title);

        [[maybe_unused]] [[nodiscard]] bool isRunning() const;

        friend std::ostream &operator<<(std::ostream &out, Menu &menu);

        void startLoop();

        Menu(std::vector<function> tasks, std::string title, std::vector<std::string> items,
             std::vector<std::string> descriptions);

    protected:
        virtual size_t identifyTheTask(const std::string &cmd);

    private:
        void printMenu(std::ostream &out);

        void userInputAction(const std::string &command);

        std::vector<function> m_tasks{};
        std::string m_title{};
        std::vector<std::string> m_items{};
        std::vector<std::string> m_descriptions{};
        bool m_running{};
    };
}
