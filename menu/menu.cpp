#include <regex>
#include <utility>
#include <vector>
#include "CLI-Autocomplete/include/autocomplete.h"
#include "menu.h"
#include "utility/constants.h"
#include "utility/utility.h"

namespace UserMenu {
    Menu::Menu(std::vector<function> tasks, std::string title, std::vector<std::string> items,
               std::vector<std::string> descriptions)
            :
            m_tasks{std::move(tasks)},
            m_title{std::move(title)},
            m_items{std::move(items)},
            m_descriptions{std::move(descriptions)} {
        m_items.emplace_back("clear");
        m_descriptions.emplace_back("Очистка экрана");
        m_items.emplace_back("exit");
        m_descriptions.emplace_back("Выход из меню");
    }

    [[maybe_unused]] bool Menu::isRunning() const {
        return m_running;
    }

    [[maybe_unused]] void Menu::setTitle(std::string title) {
        m_title = std::move(title);
    }

    void Menu::printMenu(std::ostream &out) {
        out << utility::coloredMessage(m_title, constants::title_color) << std::endl;
        for (size_t i = 0; i < m_items.size() && i < m_descriptions.size(); ++i) {
            out << m_items[i] << " - " << m_descriptions[i] << '\n';
        }
    }


    std::ostream &operator<<(std::ostream &out, Menu &menu) {
        menu.printMenu(out);
        return out;
    }

    size_t Menu::identifyTheTask(const std::string &cmd) {
        for (int i = 0; i < m_items.size(); ++i) {
            if (m_items[i] == cmd) {
                return i;
            }
        }
        return m_tasks.size();
    }

    void Menu::userInputAction(const std::string &command) {
        std::string cleaned{command};
        utility::removeSpaces(cleaned);
        switch (utility::_cmdToCode(cleaned)) {
            case constants::CMD::TASK: {
                int res = identifyTheTask(cleaned);
                if (res >= 0 && res < m_tasks.size()) {
                    std::cout << "Вывод задачи \"" << cleaned << "\":" << '\n';
                    m_tasks[res]();
                    std::cout << std::endl << std::endl;
                } else {
                    std::cerr << utility::coloredMessage("ERROR: Проверьте правильность введённой задачи",
                                                         constants::err_color) << '\n' << '\n';
                }
            }
                break;
            case constants::CMD::ERROR:
                std::cerr << utility::coloredMessage("ERROR: Что-то пошло не так, попробуйте ещё раз",
                                                     constants::err_color) << '\n' << '\n';
                break;
            case constants::CMD::EXIT:
                m_running = false;
            case constants::CMD::CLEAR:
                utility::clearScreen();
                break;
        }
    }

    void Menu::startLoop() {
        using std::cin, std::cout, std::endl, std::string;

        auto[dict, status, message] = parse_config_file(constants::config_file_path);
        if (!status) {
            std::cerr << utility::coloredMessage(message, constants::err_color) << endl;
            exit(0);
        }

        string userInput{};
        m_running = true;
        while (m_running) {
            printMenu(cout);
            userInput = input(dict, constants::line_prompt, constants::optional_brackets,
                              constants::title_color, constants::predict_color, constants::default_color);
            cout << '\n' << '\n';
            userInputAction(userInput);
        }
    }
}
