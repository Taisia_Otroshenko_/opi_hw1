#include <string>
#include <vector>
#include "menu/menu.h"
#include "tasks/tasks.h"

using namespace std;
using namespace UserMenu;
using namespace Tasks;

int main() {
    vector<Menu::function> tasks{FillMatrixRandom, OutMatrix, MaxMultiplicationInRow,
                                 Replace, SortRowsInMatrixDescending, MaxElementUnderTheSideDiagonal};
    string title{"ОПИ - ЛР1: Меню программы"};
    vector<string> items{"task fillMatrix", "task printMatrix",
                         "task maxMultiplication", "task replace",
                         "task sortRows", "task maxElement"};
    vector<string> descriptions{
            "Заполнение матрицы случайными числами от -5 до 17",
            "Вывод матрицы",
            "Поиск строки с максимальным произведением элементов (игнорируя нули)",
            "Замена всех отрицательных элементов матрицы их квадратами",
            "Сортирока всех строк матрицы по убыванию",
            "Поиск максимального элемента под побочной диагональю матрицы"
    };

    Menu menu{tasks, title, items, descriptions};

    menu.startLoop();

    return 0;
}
